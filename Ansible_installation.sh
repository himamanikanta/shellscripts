#!/bin/bash
#----------------------------------------------------
#Description : Install Ansible in ubuntu
#-----------------------------------------------------
#
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y

#!/bin/bash
#Description : To check samba server stop start
#This script works on redhat and centos only
DATE="date +%y%m%d_%H%M%S"
LOG_FILE="/tmp/${DATE}_smba_start.log"

#Check if there  smaba is installed
 if [ -!f "/usr/lib/systemd/system/smb.service" ]
 then
   echo "Samba server is not installed or service file are not added to systemctl demon" | tee -a $LOG_FILE
   echo "Exiting script as there no samba server" |tee -a $LOG_FILE
   exit
 fi 

#starting samba server
systemctl start smb.service >  $LOG_FILE
ps -ef |grep smbd |grep -v grep
 if [ $? != 0 ]
 then
   echo "Failed to start samba server" |tee -a $LOG_FILE
   echo "check log file : $LOG_FILE" 
   exit
 fi

#check if service systemctl enabled 
cmd_execute="systemctl status smb.service |grep Loaded | cut -d";"" -f2"
if [ ${cmd_execute} = disabled ]
then
   echo "Smaba server  is in disabled state"
   echo "samba server won't start when server is rebooted"
fi
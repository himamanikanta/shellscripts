#!/bin/bash
#-------------------------------------------------
#Description : It is to increase session time out 
#-------------------------------------------------
#
#To launch script 
#curl -s https://gitlab.com/himamanikanta/shellscripts/-/raw/master/Increase_Session_timeout_LINUX.sh |bash
#
echo "ServerAliveInterval 120" >> ~/.ssh/config
echo "# other configs
ClientAliveInterval 120	
ClientAliveCountMax 100" |sudo tee -a  /etc/ssh/sshd_config

#Restart ssh demon 
sudo /etc/init.d/ssh restart

echo -e "\033[5m \033[7m Please re-connect to consider changes \033[0m"
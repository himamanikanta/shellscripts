#!/bin/bash
# Description : check REDHAT releas and launch yum install 
#

FILE_LOG="/tmp/install-yum.log"

RELEASE=$(cat /etc/redhat-release | tr -d [a-z][A-Z] | tr -d " " |cut -d "." -f1)

if [ "${RELEASE}" = "6" ]
then
    yum install -y adcli sssd authconfig adcli info code1.hund.com adcli join -U code1 code1.hund.com | tee -a ${FILE_LOG}
elif [ "${RELEASE}" = "7" ]
then
    yum install -y realmd krb5 -workstation samba-common-tools sssd realm join -U code1@code1.emi.hund.com code1.emi.hund.com --verbose | tee -a ${FILE_LOG}
fi
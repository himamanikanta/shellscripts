#!/bin/bash
#-------------------------------
# Description: Auto commit git #
#-------------------------------
#Arguments :
# File with list of directories to auto commit 
#
# Basic Variables
FLAG=${HOME}/Desktop/AUTO_COMMIT_ERROR
LIST_FILE=${HOME}/Documents/DATA/Git_auto_commit.dat

#Function to exit and create flag
function EXIT_ERROR {
    RC=$1
    if [ "${RC}" != "0" ]; then
      MSG=$2
      echo "script exits with error\n ERROR: ${MSG}" > ${FLAG}
    fi
}
#Function check if internet avaliable
function INTERNET_TEST {
  ping -c 1 google.com
    EXIT_ERROR $? "NO INTERNET CONNECTION"
}

#Function to check if there is changes in git repository
function GIT_STATUS {
  GIT_VALUE=$(git status -s)
  if [ -z $GIT_VALUE ]; then
    GIT_C=NOTHING
  else
    GIT_C=TO_COMMIT
  fi
}


#----------------
#    Main
#----------------
#Check if arg file exits
rm ${FLAG}
if [ -f ${LIST_FILE} ]
then
  EXIT_ERROR 1 "File ${LIST_FILE} doesn't exist"
fi

while read FOLDER_TO_COMMIT
do
  cd ${FOLDER_TO_COMMIT}
  GIT_STATUS
  if [ "${GIT_C}" = "TO_COMMIT" ]; then
    git add . ; git commit -m "AUTO COMMIT"; git push
    EXIT_ERROR $? "Commit failed for ${FOLDER_TO_COMMIT} "
  fi
done < ${LIST_FILE}